var express = require('express');
var sql = require('./../sql');
var User = require('./../sql/col/User')
var utils = require('./../utils')
var uuid = require('node-uuid')
var md5 = require('md5')
var jwt = require('jsonwebtoken')
var router = express.Router();

/* GET users listing. */
router.get('/', function(req, res, next) {
  // res.send('respond with a resource');
  res.render('users', {
    activeindex: 3,
    role: req.session.role
  })
});
/**
* @api {POST} /user/sendCode 发送短信验证码
* @apiDescription 发送短信验证码,接收 tel 手机号参数
* @apiGroup user
* @apiParam {string} tel 手机号
* @apiSuccess {json} data
* @apiSuccessExample {json} Success-Response:
  res.send({
    code: '200',
    message: '发送短信验证码',
    code // 项目上线时要取消掉
  })
  res.send({
    code: '10002',
    message: '该用户已注册'
  })
* @apiSampleRequest /api/user/sendCode
* @apiVersion 0.0.0
*/
router.post('/sendCode', (req, res, next) => {
// 验证 手机号是不是被注册过
// 根据手机号查询用户集合，如果没有查询到记录，表示用户没有被注册过，根据 手机号以及验证码 插入一条用户的信息
// 如果查询到记录，验证账户名或者密码是不是为空，如果不为空，证明该用户已经注册过了，如果为空，表示只是发送过短信验证码，重新发送一次短信验证码，更新该手机号对应的手机验证码
  const { tel } = req.body
  sql.find(User, { tel }, { _id: 0 }).then(data1 => {
    if (data1.length > 0) {
      // 验证用户名和密码是不是为空
      if (data1[0].username === '') {
        const code = utils.randomCode()
        // 用户名为空 --- 还未真正注册
        // 发送短信验证码，更新手机号对应的验证码
        // 缺少发送短信验证码  utils.sendCode(tel, code).then(() => { 以下代码}) --- 没有发送短信的情况下如何测试

        //做了更改
        utils.sendCode(tel, code).then(() => {
          sql.update(User, { tel }, { $set: { code}}).then(() => {
            res.send({
              code: '200',
              message: '发送短信验证码',
              code1: code // 项目上线时要取消掉
            })
          })
        })
      } else {
        res.send({
          code: '10002',
          message: '该用户已注册'
        })
      }
    } else {
      // 没有查询到记录 --- 插入记录
      const code = utils.randomCode()
      const insertData = {
        userid: 'user_' + uuid.v1(),
        username: '',
        nickname: '嗨🐶' + new Date().getTime(),
        password: '',
        tel,
        email: '',
        avatar: 'https://ss3.bdstatic.com/70cFv8Sh_Q1YnxGkpoWK1HF6hhy/it/u=1020771565,2453337692&fm=26&gp=0.jpg',
        birthday: '',
        sex: -1,
        hobby: [],
        regTime: '',
        lastTime: '',
        loginState: 0,
        code
      }

      // 发送短信验证码
      utils.sendCode(tel, code).then(() => {
        sql.insert(User, insertData).then(() => {
          res.send({
            code: '200',
            message: '发送短信验证码',
            code1: code // 项目上线时要取消掉
          })
        })
      })
    }
  })
})

/**
* @api {POST} /user/register 注册
* @apiDescription 注册
* @apiGroup user
* @apiParam {string} username 账户名
* @apiParam {string} tel 手机号
* @apiParam {string} code 手机验证码
* @apiParam {string} password 密码
* @apiSuccess {json} data
* @apiSuccessExample {json} Success-Response:
  res.send({
    code: '10003',
    message: '该用户名已被占用，请重新输入'
  })
  res.send({
    code: '10004',
    message: '请先点击发送短信验证码'
  })
  res.send({
    code: '10005',
    message: '验证码错误'
  })
  res.send({
    code: '200',
    message: '注册成功'
  })
* @apiSampleRequest /api/user/register
* @apiVersion 0.0.0
*/
router.post('/register', (req, res, next) => {
  // 获取表单信息，验证用户名有没有被注册过，
  let { username, tel, password, code } = req.body
  // console.log('11111', req.body)
  sql.find(User, { username }, {_id:0}).then(data1 => {
    console.log('11111', data1)
    if (data1.length > 0) {
      // 如果注册过，提示该用户名已被占用
      res.send({
        code: '10003',
        message: '该用户名已被占用，请重新输入'
      })
    } else {
      // 如果没有被注册过，验证手机号
      // 验证手机号存不存在
      sql.find(User, { tel }, { _id: 0}).then(data2 => {
        console.log('22222', data2)
        // 如果手机号不存在，直接提醒用户先获取短信验证码
        if (data2.length === 0) {
          res.send({
            code: '10004',
            message: '请先点击发送短信验证码'
          })
        } else {
          // 如果手机号存在，继续校验验证码对不对
          sql.find(User, { tel, code }, { _id: 0}).then(data3 => {
            console.log('333333', data3)
            // 如果验证码正确
              // 再次检验 用户名是否为空，
              // 不为空代表已存在
            if (data3[0].username !== '') {
              res.send({
                code: '10033',
                message: '手机号以被使用'
              })
            } else {
              // 如果验证码不正确，提醒用户验证码错误
                if (data3.length === 0) {
                  res.send({
                    code: '10005',
                    message: '验证码错误'
                  })
              }
              if (data3[0].username !== '') {
                res.send({
                  code: '10003',
                  message: '该用户名已被占用，请重新输入'
                })
              } else {
                // 为空代表 可以注册，
                // 给密码加密，以手机号为条件，更新数据库的数据
                password = md5(password)
                sql.update(User, { tel }, { $set: {
                  username,
                  password,
                  tel,
                  regTime: new Date().getTime()
                }}).then(() => {
                  res.send({
                    code: '200',
                    message: '注册成功'
                  })
                })
              }
              
            }
          })
        }
      })
    }
  })
})


/**
* @api {POST} /api/user/login 登录
* @apiDescription 登录
* @apiGroup user
* @apiParam {string} loginname 账户名或者是手机号
* @apiParam {string} password 密码
* @apiSuccess {json} data
* @apiSuccessExample {json} Success-Response:
  res.send({
    code: '10006',
    message: '该用户还未注册'
  })
  res.send({
    code: '10007',
    message: '密码错误'
  })
  res.send({
    code: '200',
    message: '登录成功',
    data: {
      userid: data2[0].userid // 编写获取用户信息的依据
    }
  })
* @apiSampleRequest /api/user/login
* @apiVersion 0.0.0
*/
router.post('/login', (req, res, next) => {
  // 接收 表单的数据 （账户名或者是手机号 - loginname + password）
  let { loginname, password } = req.body
  sql.find(User, { $or: [{ username: loginname }, { tel: loginname }]}, { _id: 0}).then(data1 => {
    // 验证 账户名或者是手机号存在不存在
    if (data1.length === 0 || data1[0].username === '') {
      res.send({
        code: '10006',
        message: '该用户还未注册'
      })
    } else {
      // 再验证密码匹配
      password = md5(password)
      sql.find(User, { $or: [{ username: loginname }, { tel: loginname }], password}, { _id: 0}).then(data2 => {
        // 再验证密码匹配
        if (data2.length === 0) {
          res.send({
            code: '10007',
            message: '密码错误'
          })
        } else {
          // 密码匹配完成，记录登录时间，记录登录的状态
          sql.update(User, { userid: data2[0].userid }, {
            $set: {
              lastTime: new Date().getTime(),
              loginState: 1 // 显示当前的账户是在线状态
            }
          }).then(() => {
            // 生成token
            // jwt.sign(token生成的字段， 密钥， 参数)
            const token = jwt.sign({ userid: data2[0].userid}, '大勋勋', {
              expiresIn: 7 * 24 * 60 * 60 // 单位为秒
              // expiresIn: 30
            })
            // 登录之后给前端返回登录信息
            res.send({
              code: '200',
              message: '登录成功',
              data: {
                userid: data2[0].userid, // 编写获取用户信息的依据
                token: token
              }
            })
          })
        }
      })
    }
  })

})

/**
* @api {GET} /api/user/getUserInfo 获取用户信息
* @apiDescription 获取用户信息
* @apiGroup user
* @apiParam {string} userid 用户id
* @apiParam {string} token  使用时通过头信息可以传递，可以get
* @apiSuccess {json} data
* @apiSuccessExample {json} Success-Response:
 res.send({
    code: '10119',
    message: 'token校验失败'
  })
  res.send({
    code: '200',
    message: '用户信息',
    data: data[0]
  })
* @apiSampleRequest /api/user/getUserInfo
* @apiVersion 0.0.0
*/
router.get('/getUserInfo', (req, res, next) => {
  utils.validateToekn(req).then(() => {
    const { userid } = req.query
    sql.find(User, { userid }, { 
      _id: 0, 
      username: 1, 
      nickname: 1, 
      tel: 1,
      avatar: 1, 
      hobby: 1, 
      email: 1, 
      sex: 1, 
      birthday: 1
    }).then(data => {
      res.send({
        code: '200',
        message: '用户信息',
        data: data[0]
      })
    })
  }).catch(() => {
    res.send({
      code: '10119',
      message: 'token校验失败'
    })
  })
})

module.exports = router;
