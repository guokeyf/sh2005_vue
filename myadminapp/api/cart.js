var express = require('express');
var sql = require('./../sql')
var Cart = require('./../sql/col/Cart')
var Product = require('./../sql/col/Product')
var utils = require('./../utils/index')
var uuid = require('node-uuid')
var router = express.Router();

/**
* @api {GET} /api/cart 获取购物车数据
* @apiDescription 获取购物车数据
* @apiGroup cart
* @apiParam {string} userid 用户的id
* @apiSuccess {json} data
* @apiSuccessExample {json} Success-Response:
  {
    code: '200',
    message: '获取购物车数据',
    data
  }
* @apiSampleRequest /api/cart
* @apiVersion 0.0.0
*/
router.get('/', function(req, res, next) {
  const { userid } = req.query
  utils.validateToken(req).then(() => {
    sql.sort(Cart, {userid}, {_id:0}, {time: -1}).then(data => {
      res.send({
        code: '200',
        message: '获取商品数据',
        data
      })
    })
  }).catch(() => {
    res.send({
      code: '10119',
      message: '请先登录'
    })
  });
})

/**
* @api {POST} /api/cart/add 加入购物车
* @apiDescription 加入购物车
* @apiGroup cart
* @apiParam {string} userid 用户的id
* @apiParam {string} proid 产品的id
* @apiParam {string} num 数量
* @apiSuccess {json} data
* @apiSuccessExample {json} Success-Response:
  res.send({
    code: '10030',
    message: '没有查找到该商品的任何信息'
  )
  res.send({
    code: '10031',
    message: '加入购物车成功'
  })
  res.send({
    code: '10032',
    message: '库存不足'
  })
* @apiSampleRequest /api/cart
* @apiVersion 0.0.0
*/

router.post('/add', function (req, res, next) {
  let { userid, proid, num } = req.body
  num *= 1
  // 此处无需验证 用户是否存在 --- 生成token 依赖 userid，如果不存在 token验证不通过
  // 验证商品存在不存在
  sql.find(Product, { proid }, { _id: 0}).then(data1 => {
    if (data1.length === 0) {
      res.send({
        code: '10030',
        message: '没有查找到该商品的任何信息'
      })
    } else {
      // 提取  proname ， proimg ，price ，stock
      const { proname, proimg, price, stock } = data1[0]
      const time = new Date().getTime() + ''
      // 如果库存小于加入购物车的数据，显示库存不足
      if (stock < num) {
        res.send({
          code: '10032',
          message: '库存不足'
        })
        return
      }
      // 根据 userid 和proid 定位有没有该商品
      sql.find(Cart, { userid, proid }, { _id: 0 }).then(data2 => {
        if (data2.length === 0) {
          // 该商品没有被加入到该用户的购物车
          sql.insert(Cart, {
            cartid: 'cart_' + uuid.v1(),
            userid,
            proid,
            flag: 1,
            proname,
            proimg,
            price,
            stock,
            num,
            time
          }).then(() => {
            res.send({
              code: '10031',
              message: '加入购物车成功'
            })
          })
        } else {
          // 更新数据库中的 数量
          sql.update(Cart, { userid, proid }, { $set:{ time }, $inc: { num: num}}).then(() => {
            res.send({
              code: '10031',
              message: '加入购物车成功'
            })
          })
        }
      })
    }
  })
})

/**
* @api {GET} /api/cart/delete 删除数据
* @apiDescription 删除数据
* @apiGroup cart
* @apiParam {string} cartid 购物车数据id
* @apiSuccess {json} data
* @apiSuccessExample {json} Success-Response:
  {
    code: '200',
    message: '删除数据',
  }
  res.send({
    code: '10119',
    message: '请先登录',
  })
* @apiSampleRequest /api/cart/delete
* @apiVersion 0.0.0
*/
router.get('/delete', function(req, res, next) {
  const { cartid } = req.query
  utils.validateToken(req).then(() => {
    sql.delete(Cart, {cartid}).then(() => {
      res.send({
        code: '200',
        message: '获取商品数据'
      })
    })
  }).catch(() => {
    res.send({
      code: '10119',
      message: '请先登录'
    })
  });
})

/**
* @api {GET} /api/cart/update 更新数据库
* @apiDescription 更新数据库
* @apiGroup cart
* @apiParam {string} cartid 购物车数据id
* @apiParam {number} num 商品数量 
* @apiParam {string} token token值 
* @apiSuccess {json} data
* @apiSuccessExample {json} Success-Response:
  {
    code: '200',
    message: '更新数据库成功',
  }
  res.send({
    code: '10119',
    message: '请先登录',
  })
* @apiSampleRequest /api/cart/update
* @apiVersion 0.0.0
*/
router.get('/update', function(req, res, next) {
  let { cartid, num } = req.query
  num *= 1
  utils.validateToken(req).then(() => {
    sql.update(Cart, {cartid}, {$set: {num}}).then(() => {
      res.send({
        code: '200',
        message: '更新数据库'
      })
    })
  }).catch(() => {
    res.send({
      code: '10119',
      message: '请先登录'
    })
  })
})

/**
* @api {GET} /api/cart/updateAllFlag 更新购物车所有的数据被选中
* @apiDescription 更新购物车所有的数据被选中
* @apiGroup cart
* @apiParam {string} userid 用户id
* @apiParam {number} num 1 标示全部选中 0 表示全部不选中 
* @apiSuccess {json} data
* @apiSuccessExample {json} Success-Response:
  {
    code: '200',
    message: '更新数据库成功',
  }
  res.send({
    code: '10119',
    message: '请先登录',
  })
* @apiSampleRequest /api/cart/updateAllFlag
* @apiVersion 0.0.0
*/
router.get('/updateAllFlag', function(req, res, next) {
  let { userid, num } = req.query
  num *= 1
  utils.validateToken(req).then(() => {
    sql.update(Cart, {userid}, {$set: {flag: num}}, 1).then(() => {
      res.send({
        code: '200',
        message: '更新数据库成功'
      })
    })
  }).catch(() => {
    res.send({
      code: '10119',
      message: '请先登录'
    })
  });
})

/**
* @api {GET} /api/cart/updateItemFlag 更新单个购物车数据被选中
* @apiDescription 更新单个购物车数据被选中
* @apiGroup cart
* @apiParam {string} cartid 购物车id
* @apiParam {number} num 1 表示全部选中 0 表示全部不选中 
* @apiSuccess {json} data
* @apiSuccessExample {json} Success-Response:
  {
    code: '200',
    message: '更新数据库成功',
  }
  res.send({
    code: '10119',
    message: '请先登录',
  })
* @apiSampleRequest /api/cart/updateItemFlag
* @apiVersion 0.0.0
*/
router.get('/updateItemFlag', function(req, res, next) {
  let { cartid, num } = req.query
  num *= 1
  utils.validateToken(req).then(() => {
    sql.update(Cart, {cartid}, {$set: {flag: num}}).then(() => {
      res.send({
        code: '200',
        message: '更新数据库成功'
      })
    })
  }).catch(() => {
    res.send({
      code: '10119',
      message: '请先登录'
    })
  });
})


module.exports = router;
