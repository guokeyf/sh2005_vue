var express = require('express');
var sql = require('./../sql/index');
var Product = require('./../sql/col/Product');
var router = express.Router();
// 请求产品的列表数据 - 分页 - 移动端分页 - 上啦加载
/**
* @api {GET} /api/pro 请求商品的列表数据
* @apiDescription 请求商品的列表数据,接收 limit 和 count 参数，limit代表每页显示的个数，默认值为10，count 表示页码，从1开始
* @apiGroup product
* @apiParam {number} limit 每页显示个数默认为10
* @apiParam {number} count 页码默认为1
* @apiSuccess {json} data
* @apiSuccessExample {json} Success-Response:
  {
    code: '200',
    message: '请求商品的列表数据',
    data
  }
* @apiSampleRequest /api/pro
* @apiVersion 0.0.0
*/
router.get('/', function(req, res, next) {
  // 每页显示的个数 + 页码
  let { limit, count } = req.query
  // limit = limit === '' ? 10 : limit * 1 // 每页显示个数
  // count = count === '' ? 0 : count - 1 // 前提条件是前端传递的第一页数据的 值为 1，如果为0 则不减 1
  limit = limit * 1 || 10
  count = count * 1 || 0
  sql.paging(Product, {}, { _id: 0}, limit, count).then(data => {
    res.send({
      code: '200',
      message: '请求商品的列表数据',
      data
    })
  })
});


/**
* @api {GET} /api/pro/getCategory 获取商品的分类
* @apiDescription 获取商品的分类
* @apiGroup product
* @apiSuccess {json} data
* @apiSuccessExample {json} Success-Response:
  {
    code: '200',
    message: '获取商品的分类',
    data
  }
* @apiSampleRequest /api/pro/getCategory
* @apiVersion 0.0.0
*/
router.get('/getCategory', function(req, res, next) {
  sql.distinct(Product, 'category').then(data => {
    res.send({
      code: '200',
      message: '获取商品的分类',
      data
    })
  })
});

/**
* @api {GET} /api/pro/detail 请求商品的详情数据
* @apiDescription 请求商品的详情数据,接收 proid，此值为产品的唯一值
* @apiGroup product
* @apiParam {string} proid
* @apiSuccess {json} data
* @apiSuccessExample {json} Success-Response:
  {
    code: '200',
    message: '请求商品的详情数据',
    data
  }
* @apiSampleRequest /api/pro/detail
* @apiVersion 0.0.0
*/
router.get('/detail', function(req, res, next) {
  const { proid } = req.query
  sql.find(Product, { proid }, { _id: 0 }).then(data => {
    data.length === 0 ? res.send({
      code: '10001',
      message: '查无此产品'
    }) : res.send({
      code: '200',
      message: '请求商品的详情数据',
      data: data[0]
    })
  })
});
module.exports = router;
