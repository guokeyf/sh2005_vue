var express = require('express');
var sql = require('./../sql')
var Banner = require('./../sql/col/Banner')
var router = express.Router();

/**
* @api {GET} /api/banner 获取轮播图数据//发动机萨芬的
* @apiDescription 获取轮播图数据
* @apiGroup banner
* @apiSuccess {json} data
* @apiSuccessExample {json} Success-Response:
  {
    code: '200',
    message: '获取轮播图数据',
    data
  }
* @apiSampleRequest /api/banner
* @apiVersion 0.0.0
*/
router.get('/', function(req, res, next) {
  sql.find(Banner, {}, {_id:0}).then(data => {
    res.send({
      code: '200',
      message: '获取轮播图数据',
      data
    })
  })
});

module.exports = router;
