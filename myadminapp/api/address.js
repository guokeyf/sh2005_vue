var express = require('express');
var sql = require('../sql')
var Address = require('../sql/col/Address')
var uuid = require('node-uuid')
var utils = require('./../utils/index')
var router = express.Router();

/**
* @api {GET} /api/address 获取地址列表
* @apiDescription 获取地址列表
* @apiGroup address
* @apiParam { string} userid 用户id
* @apiSuccess {json} data
* @apiSuccessExample {json} Success-Response:
  {
    code: '200',
    message: '获取地址列表',
    data
  }
* @apiSampleRequest /api/address
* @apiVersion 0.0.0
*/
router.get('/', function(req, res, next) {
  const { userid } = req.query
  utils.validateToken(req).then(() => {
    sql.find(Address, {userid}, {_id:0}).then(data => {
      res.send({
        code: '200',
        message: '获取地址列表',
        data
      })
    })
  }).catch(() => {
    res.send({
      code: '10119',
      message: '请先登录'
    })
  })
  
});
/**
* @api {GET} /api/address/default 获取默认地址
* @apiDescription 获取默认地址
* @apiGroup address
* @apiParam { string} userid 用户id
* @apiSuccess {json} data
* @apiSuccessExample {json} Success-Response:
  {
    code: '200',
    message: '获取默认地址',
    data
  }
* @apiSampleRequest /api/address/default
* @apiVersion 0.0.0
*/
router.get('/default', function(req, res, next) {
  const { userid } = req.query
  utils.validateToken(req).then(() => {
    sql.find(Address, {userid, isDefault: 1}, {_id:0}).then(data => {
      res.send({
        code: '200',
        message: '获取默认地址',
        data
      })
    })
  }).catch(() => {
    res.send({
      code: '10119',
      message: '请先登录'
    })
  })
  
});

/**
* @api {POST} /api/address/add 添加地址
* @apiDescription 添加地址
* @apiGroup address
* @apiParam { string} userid 用户id
* @apiParam { string} userid 用户id
* @apiSuccess {json} data
* @apiSuccessExample {json} Success-Response:
  {
    code: '200',
    message: '添加地址',
    data
  }
* @apiSampleRequest /api/address/add
* @apiVersion 0.0.0
*/
router.post('/add', function(req, res, next) {
  const obj = req.body
  obj.addressid = "address_" + uuid.v1()
  obj.isDefault *= 1
  utils.validateToken(req).then(() => {
      if (obj.isDefault == 1) {
        sql.update(Address, {}, {$set: {isDefault: 0}}, 1).then(() => {
          sql.insert(Address, obj).then(() => {
            res.send({
              code: '200',
              message: '添加地址'
            })
          })
        })
      } else {
        sql.insert(Address, obj).then(() => {
          res.send({
            code: '200',
            message: '添加地址'
          })
        })
      }
  }).catch(() => {
    res.send({
      code: '10119',
      message: '请先登录'
    })
  })
  
});

module.exports = router;