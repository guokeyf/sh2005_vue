var express = require('express');
var sql = require('./../sql')
var Order = require('./../sql/col/Order')
var Cart = require('./../sql/col/Cart')
var utils = require('./../utils/index')
var uuid = require('node-uuid')
var router = express.Router();

/**
* @api {POST} /api/order/add 添加订单
* @apiDescription 添加订单
* @apiGroup order
* @apiParam {string} userid 用户的id
* @apiParam {string} token token
* @apiParam {string} arrStr 选中的购物车数据字符串(内部是数组)
* @apiSuccess {json} data
* @apiSuccessExample {json} Success-Response:
  {
    code: '200',
    message: '添加订单',
    time
  }
* @apiSampleRequest /api/order/add
* @apiVersion 0.0.0
*/
function getOrderId () {
  return 'order_' + new Date().getTime() + Math.floor(Math.random()*10000)
}
router.post('/add', function(req, res, next) {
  console.log(55555555555)
  const { userid, arrStr } = req.body
  utils.validateToken(req).then(() => {
    // 字符串
    const arr = JSON.parse(arrStr)
    // 时间
    const time = new Date().getTime()
    arr.map(item => {
      item.orderid = getOrderId()
      item.userid = userid
      item.username = '' // 默认地址的用户名 
      item.tel = '' // 默认地址的电话
      item.address = '' // 默认地址
      item.state = 0
      item.time = time
    })
  sql.insert(Order, arr, { _id: 0}).then(() => {
    res.send({
      'code': '200',
      message: '生成订单',
      time // 填写订单的页面获取数据靠的是时间
    })
  })
  }).catch(() => {
    res.send({
      code: '10119',
      message: '请先登录'
    })
  })
})

/**
* @api {POST} /api/order/getTimeOrder 获取一笔订单信息列表
* @apiDescription 获取一笔订单信息列表
* @apiGroup order
* @apiParam {string} userid userid
* @apiParam {string} time 时间
* @apiSuccess {json} data
* @apiSuccessExample {json} Success-Response:
  {
    code: '200',
    message: '更新数据库成功',
  }
  res.send({
    code: '10119',
    message: '请先登录',
  })
* @apiSampleRequest /api/order/getTimeOrder
* @apiVersion 0.0.0
*/
router.post('/getTimeOrder', function(req, res, next) {
  let { userid, time } = req.body
  utils.validateToken(req).then(() => {
    sql.find(Order, { userid, time }, { _id: 0 }).then(data => {
      res.send({
        code: '200',
        message: '获取一笔订单信息列表',
        data
      })
    })
  }).catch(() => {
    res.send({
      code: '10119',
      message: '请先登录'
    })
  });
})

/**
* @api {POST} /api/order/deleteCartData 删除购物车选中的数据
* @apiDescription 删除购物车选中的数据
* @apiGroup order
* @apiParam {string} orderarrStr 由购物车cartid 组成的一个数组 
* @apiSuccess {json} data
* @apiSuccessExample {json} Success-Response:
  {
    code: '200',
    message: '删除购物车选中的数据'
  }
* @apiSampleRequest /api/order/deleteCartData
* @apiVersion 0.0.0
*/
router.post('/deleteCartData', (req, res, next) => {
  const { orderarrStr } = req.body // orderarrStr 由购物车id生成的一个数组
  utils.validateToken(req).then(() => {
    const orderarr = JSON.parse(orderarrStr)
    const arr = []
    orderarr.forEach(item => {
      arr.push(new Promise(resolve => {
        sql.delete(Cart, { cartid: item }).then(() => {
          resolve()
        })
      }))
    })
    // https://www.jianshu.com/p/5ad8ea974c3d
    Promise.all(arr).then(() => {
      res.send({
        code: '200',
        message: '删除购物车选中的数据'
      })
    })
  }).catch(() => {
    res.send({
      code: '10119',
      message: '请先登录'
    })
  })
})

/**
* @api {POST} /api/order/updateOrderAddress 更新订单的地址
* @apiDescription 更新订单的地址
* @apiGroup order
* @apiParam {string} userid 
* @apiParam {string} time 时间
* @apiParam {string} username 收货人姓名
* @apiParam {string} tel 收货人电话
* @apiParam {string} address 收货人地址
* @apiSuccess {json} data
* @apiSuccessExample {json} Success-Response:
  {
    code: '200',
    message: '更新订单的地址',
    data
  }
* @apiSampleRequest /api/order/updateOrderAddress
* @apiVersion 0.0.0
*/
router.post('/updateOrderAddress', (req, res, next) => {
  const { userid, time, username, tel, address } = req.body
  utils.validateToekn(req).then(() => {
    sql.update(Order, { userid, time }, { $set: { username, tel, address }}, 1).then(() => {
      res.send({
        code: '200',
        message: '更新订单的地址'
      })
    })
  }).catch(() => {
    res.send({
      code: '10119',
      message: '请先登录'
    })
  })
})
module.exports = router;
