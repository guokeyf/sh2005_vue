define({ "api": [
  {
    "type": "GET",
    "url": "/api/address",
    "title": "获取地址列表",
    "description": "<p>获取地址列表</p>",
    "group": "address",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "userid",
            "description": "<p>用户id</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "data",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n  code: '200',\n  message: '获取地址列表',\n  data\n}",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/api/address"
      }
    ],
    "version": "0.0.0",
    "filename": "api/address.js",
    "groupTitle": "address",
    "name": "GetApiAddress"
  },
  {
    "type": "GET",
    "url": "/api/address/default",
    "title": "获取默认地址",
    "description": "<p>获取默认地址</p>",
    "group": "address",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "userid",
            "description": "<p>用户id</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "data",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n  code: '200',\n  message: '获取默认地址',\n  data\n}",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/api/address/default"
      }
    ],
    "version": "0.0.0",
    "filename": "api/address.js",
    "groupTitle": "address",
    "name": "GetApiAddressDefault"
  },
  {
    "type": "POST",
    "url": "/api/address/add",
    "title": "添加地址",
    "description": "<p>添加地址</p>",
    "group": "address",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "userid",
            "description": "<p>用户id</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "data",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n  code: '200',\n  message: '添加地址',\n  data\n}",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/api/address/add"
      }
    ],
    "version": "0.0.0",
    "filename": "api/address.js",
    "groupTitle": "address",
    "name": "PostApiAddressAdd"
  },
  {
    "type": "GET",
    "url": "/api/banner",
    "title": "获取轮播图数据",
    "description": "<p>获取轮播图数据</p>",
    "group": "banner",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "data",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n  code: '200',\n  message: '获取轮播图数据',\n  data\n}",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/api/banner"
      }
    ],
    "version": "0.0.0",
    "filename": "api/banner.js",
    "groupTitle": "banner",
    "name": "GetApiBanner"
  },
  {
    "type": "GET",
    "url": "/api/cart",
    "title": "获取购物车数据",
    "description": "<p>获取购物车数据</p>",
    "group": "cart",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "userid",
            "description": "<p>用户的id</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "data",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n  code: '200',\n  message: '获取购物车数据',\n  data\n}",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/api/cart"
      }
    ],
    "version": "0.0.0",
    "filename": "api/cart.js",
    "groupTitle": "cart",
    "name": "GetApiCart"
  },
  {
    "type": "GET",
    "url": "/api/cart/delete",
    "title": "删除数据",
    "description": "<p>删除数据</p>",
    "group": "cart",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "cartid",
            "description": "<p>购物车数据id</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "data",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n  code: '200',\n  message: '删除数据',\n}\nres.send({\n  code: '10119',\n  message: '请先登录',\n})",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/api/cart/delete"
      }
    ],
    "version": "0.0.0",
    "filename": "api/cart.js",
    "groupTitle": "cart",
    "name": "GetApiCartDelete"
  },
  {
    "type": "GET",
    "url": "/api/cart/update",
    "title": "更新数据库",
    "description": "<p>更新数据库</p>",
    "group": "cart",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "cartid",
            "description": "<p>购物车数据id</p>"
          },
          {
            "group": "Parameter",
            "type": "number",
            "optional": false,
            "field": "num",
            "description": "<p>商品数量</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "token",
            "description": "<p>token值</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "data",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n  code: '200',\n  message: '更新数据库成功',\n}\nres.send({\n  code: '10119',\n  message: '请先登录',\n})",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/api/cart/update"
      }
    ],
    "version": "0.0.0",
    "filename": "api/cart.js",
    "groupTitle": "cart",
    "name": "GetApiCartUpdate"
  },
  {
    "type": "GET",
    "url": "/api/cart/updateAllFlag",
    "title": "更新购物车所有的数据被选中",
    "description": "<p>更新购物车所有的数据被选中</p>",
    "group": "cart",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "userid",
            "description": "<p>用户id</p>"
          },
          {
            "group": "Parameter",
            "type": "number",
            "optional": false,
            "field": "num",
            "description": "<p>1 标示全部选中 0 表示全部不选中</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "data",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n  code: '200',\n  message: '更新数据库成功',\n}\nres.send({\n  code: '10119',\n  message: '请先登录',\n})",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/api/cart/updateAllFlag"
      }
    ],
    "version": "0.0.0",
    "filename": "api/cart.js",
    "groupTitle": "cart",
    "name": "GetApiCartUpdateallflag"
  },
  {
    "type": "GET",
    "url": "/api/cart/updateItemFlag",
    "title": "更新单个购物车数据被选中",
    "description": "<p>更新单个购物车数据被选中</p>",
    "group": "cart",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "cartid",
            "description": "<p>购物车id</p>"
          },
          {
            "group": "Parameter",
            "type": "number",
            "optional": false,
            "field": "num",
            "description": "<p>1 表示全部选中 0 表示全部不选中</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "data",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n  code: '200',\n  message: '更新数据库成功',\n}\nres.send({\n  code: '10119',\n  message: '请先登录',\n})",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/api/cart/updateItemFlag"
      }
    ],
    "version": "0.0.0",
    "filename": "api/cart.js",
    "groupTitle": "cart",
    "name": "GetApiCartUpdateitemflag"
  },
  {
    "type": "POST",
    "url": "/api/cart/add",
    "title": "加入购物车",
    "description": "<p>加入购物车</p>",
    "group": "cart",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "userid",
            "description": "<p>用户的id</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "proid",
            "description": "<p>产品的id</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "num",
            "description": "<p>数量</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "data",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "res.send({\n  code: '10030',\n  message: '没有查找到该商品的任何信息'\n)\nres.send({\n  code: '10031',\n  message: '加入购物车成功'\n})\nres.send({\n  code: '10032',\n  message: '库存不足'\n})",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/api/cart"
      }
    ],
    "version": "0.0.0",
    "filename": "api/cart.js",
    "groupTitle": "cart",
    "name": "PostApiCartAdd"
  },
  {
    "type": "POST",
    "url": "/api/order/add",
    "title": "添加订单",
    "description": "<p>添加订单</p>",
    "group": "order",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "userid",
            "description": "<p>用户的id</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "token",
            "description": "<p>token</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "arrStr",
            "description": "<p>选中的购物车数据字符串(内部是数组)</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "data",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n  code: '200',\n  message: '添加订单',\n  time\n}",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/api/order/add"
      }
    ],
    "version": "0.0.0",
    "filename": "api/order.js",
    "groupTitle": "order",
    "name": "PostApiOrderAdd"
  },
  {
    "type": "POST",
    "url": "/api/order/getTimeOrder",
    "title": "获取一笔订单信息列表",
    "description": "<p>获取一笔订单信息列表</p>",
    "group": "order",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "userid",
            "description": "<p>userid</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "time",
            "description": "<p>时间</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "data",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n  code: '200',\n  message: '更新数据库成功',\n}\nres.send({\n  code: '10119',\n  message: '请先登录',\n})",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/api/order/getTimeOrder"
      }
    ],
    "version": "0.0.0",
    "filename": "api/order.js",
    "groupTitle": "order",
    "name": "PostApiOrderGettimeorder"
  },
  {
    "type": "POST",
    "url": "/api/order/updateOrderAddress",
    "title": "更新订单的地址",
    "description": "<p>更新订单的地址</p>",
    "group": "order",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "userid",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "time",
            "description": "<p>时间</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "username",
            "description": "<p>收货人姓名</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "tel",
            "description": "<p>收货人电话</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "address",
            "description": "<p>收货人地址</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "data",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n  code: '200',\n  message: '更新订单的地址',\n  data\n}",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/api/order/updateOrderAddress"
      }
    ],
    "version": "0.0.0",
    "filename": "api/order.js",
    "groupTitle": "order",
    "name": "PostApiOrderUpdateorderaddress"
  },
  {
    "type": "POST",
    "url": "/api/order/updateOrderAddress",
    "title": "更新订单的地址",
    "description": "<p>更新订单的地址</p>",
    "group": "order",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "userid",
            "description": ""
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "time",
            "description": "<p>时间</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "username",
            "description": "<p>收货人姓名</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "tel",
            "description": "<p>收货人电话</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "address",
            "description": "<p>收货人地址</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "data",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n  code: '200',\n  message: '更新订单的地址',\n  data\n}",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/api/order/updateOrderAddress"
      }
    ],
    "version": "0.0.0",
    "filename": "api/order.js",
    "groupTitle": "order",
    "name": "PostApiOrderUpdateorderaddress"
  },
  {
    "type": "GET",
    "url": "/api/pro",
    "title": "请求商品的列表数据",
    "description": "<p>请求商品的列表数据,接收 limit 和 count 参数，limit代表每页显示的个数，默认值为10，count 表示页码，从1开始</p>",
    "group": "product",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "number",
            "optional": false,
            "field": "limit",
            "description": "<p>每页显示个数默认为10</p>"
          },
          {
            "group": "Parameter",
            "type": "number",
            "optional": false,
            "field": "count",
            "description": "<p>页码默认为1</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "data",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n  code: '200',\n  message: '请求商品的列表数据',\n  data\n}",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/api/pro"
      }
    ],
    "version": "0.0.0",
    "filename": "api/product.js",
    "groupTitle": "product",
    "name": "GetApiPro"
  },
  {
    "type": "GET",
    "url": "/api/pro/detail",
    "title": "请求商品的详情数据",
    "description": "<p>请求商品的详情数据,接收 proid，此值为产品的唯一值</p>",
    "group": "product",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "proid",
            "description": ""
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "data",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n  code: '200',\n  message: '请求商品的详情数据',\n  data\n}",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/api/pro/detail"
      }
    ],
    "version": "0.0.0",
    "filename": "api/product.js",
    "groupTitle": "product",
    "name": "GetApiProDetail"
  },
  {
    "type": "GET",
    "url": "/api/pro/getCategory",
    "title": "获取商品的分类",
    "description": "<p>获取商品的分类</p>",
    "group": "product",
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "data",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "{\n  code: '200',\n  message: '获取商品的分类',\n  data\n}",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/api/pro/getCategory"
      }
    ],
    "version": "0.0.0",
    "filename": "api/product.js",
    "groupTitle": "product",
    "name": "GetApiProGetcategory"
  },
  {
    "type": "GET",
    "url": "/api/user/getUserInfo",
    "title": "获取用户信息",
    "description": "<p>获取用户信息</p>",
    "group": "user",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "userid",
            "description": "<p>用户id</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "token",
            "description": "<p>使用时通过头信息可以传递，可以get</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "data",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "res.send({\n   code: '10119',\n   message: 'token校验失败'\n })\n res.send({\n   code: '200',\n   message: '用户信息',\n   data: data[0]\n })",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/api/user/getUserInfo"
      }
    ],
    "version": "0.0.0",
    "filename": "api/user.js",
    "groupTitle": "user",
    "name": "GetApiUserGetuserinfo"
  },
  {
    "type": "POST",
    "url": "/api/user/login",
    "title": "登录",
    "description": "<p>登录</p>",
    "group": "user",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "loginname",
            "description": "<p>账户名或者是手机号</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "password",
            "description": "<p>密码</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "data",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "res.send({\n  code: '10006',\n  message: '该用户还未注册'\n})\nres.send({\n  code: '10007',\n  message: '密码错误'\n})\nres.send({\n  code: '200',\n  message: '登录成功',\n  data: {\n    userid: data2[0].userid // 编写获取用户信息的依据\n  }\n})",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/api/user/login"
      }
    ],
    "version": "0.0.0",
    "filename": "api/user.js",
    "groupTitle": "user",
    "name": "PostApiUserLogin"
  },
  {
    "type": "POST",
    "url": "/user/register",
    "title": "注册",
    "description": "<p>注册</p>",
    "group": "user",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "username",
            "description": "<p>账户名</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "tel",
            "description": "<p>手机号</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "code",
            "description": "<p>手机验证码</p>"
          },
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "password",
            "description": "<p>密码</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "data",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "res.send({\n  code: '10003',\n  message: '该用户名已被占用，请重新输入'\n})\nres.send({\n  code: '10004',\n  message: '请先点击发送短信验证码'\n})\nres.send({\n  code: '10005',\n  message: '验证码错误'\n})\nres.send({\n  code: '200',\n  message: '注册成功'\n})",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/api/user/register"
      }
    ],
    "version": "0.0.0",
    "filename": "api/user.js",
    "groupTitle": "user",
    "name": "PostUserRegister"
  },
  {
    "type": "POST",
    "url": "/user/sendCode",
    "title": "发送短信验证码",
    "description": "<p>发送短信验证码,接收 tel 手机号参数</p>",
    "group": "user",
    "parameter": {
      "fields": {
        "Parameter": [
          {
            "group": "Parameter",
            "type": "string",
            "optional": false,
            "field": "tel",
            "description": "<p>手机号</p>"
          }
        ]
      }
    },
    "success": {
      "fields": {
        "Success 200": [
          {
            "group": "Success 200",
            "type": "json",
            "optional": false,
            "field": "data",
            "description": ""
          }
        ]
      },
      "examples": [
        {
          "title": "Success-Response:",
          "content": "res.send({\n  code: '200',\n  message: '发送短信验证码',\n  code // 项目上线时要取消掉\n})\nres.send({\n  code: '10002',\n  message: '该用户已注册'\n})",
          "type": "json"
        }
      ]
    },
    "sampleRequest": [
      {
        "url": "/api/user/sendCode"
      }
    ],
    "version": "0.0.0",
    "filename": "api/user.js",
    "groupTitle": "user",
    "name": "PostUserSendcode"
  }
] });
