const mongoose = require('../db') 
const Schema = mongoose.Schema;

// 设置数据库看集合的字段以及数据类型
const schema = new Schema({
  cartid: { type: String },
  userid: { type: String },
  proid: { type: String },
  flag: { type: Number },
  proname: { type: String },
  price: { type: Number },
  proimg: { type: String },
  stock: { type: Number },
  num: { type: Number },
  time: { type: String }
})

module.exports = mongoose.model('Cart', schema)