const mongoose = require('../db') 
const Schema = mongoose.Schema;

// 设置数据库看集合的字段以及数据类型
const schema = new Schema({
  orderid: { type: String },
  userid: { type: String },
  cartid: { type: String }, // 提交订单删除购物车相关数据的依据
  time: { type: String }, // 生成订单的时间
  username: { type: String },
  tel: { type: String },
  address: { type: String },
  proname: { type: String },
  proimg: { type: String },
  proid: { type: String },
  price: { type: Number },
  num: { type: Number },
  state: { type: Number }, // 0 待付款 1 待发货   2 待收货   3 待评价 4 已完成
})

module.exports = mongoose.model('Order', schema)