const mongoose = require('../db') 
const Schema = mongoose.Schema;

// 设置数据库看集合的字段以及数据类型
const schema = new Schema({
  userid: { type: String },
  username: { type: String }, // 账户
  nickname: { type: String }, // 昵称
  password: { type: String }, // 密码
  tel: { type: String }, // 手机号
  email: { type: String }, // 邮箱
  avatar: { type: String }, // 头像
  birthday: { type: String }, // 生日 - 时间戳
  sex: { type: Number }, // 1 男 0 女
  hobby: { type: Array }, // 兴趣爱好
  regTime: { type: String }, // 注册时间
  lastTime: { type: String }, // 上一次登录的截止时间，注册时与注册时间一致
  loginState: { type: Number }, // 1 已登录  0 未登录
  code: { type: String } // 短信验证码
})

module.exports = mongoose.model('User', schema)

// apidoc -i api/ -o public