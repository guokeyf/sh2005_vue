const mongoose = require('../db') 
const Schema = mongoose.Schema;

// 设置数据库看集合的字段以及数据类型
const schema = new Schema({
  adminid: { type: String },
  adminname: { type: String },
  password: { type: String },
  role: { type: Number }, // 1 普通管理员  2 超级管理员
  avatar: { type: String }, // 头像 
  lasttime: { type: String } // 最近一次的登录时间
})

module.exports = mongoose.model('Admin', schema)