const mongoose = require('../db') 
const Schema = mongoose.Schema;

// 设置数据库看集合的字段以及数据类型
const schema = new Schema({
  bannerid: { type: String },
  href: { type: String },
  img: { type: String },
  alt: { type: String }
})

module.exports = mongoose.model('Banner', schema)