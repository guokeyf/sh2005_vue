const mongoose = require('../db') 
const Schema = mongoose.Schema;

// 设置数据库看集合的字段以及数据类型
const schema = new Schema({
  addressid: { type: String }, // 地址id
  userid: { type: String },
  addressDetail: { type: String }, // 详细地址
  areaCode: { type: String }, // 地址编码
  city: { type: String }, // 头像 
  county: { type: String }, // 县区
  isDefault: { type: Number }, // 是不是默认地址 1为默认 0 不是
  name: { type: String }, // 收货人姓名
  province: { type: String }, // 省
  tel: { type: String }
})

module.exports = mongoose.model('Address', schema)