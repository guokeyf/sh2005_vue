const mongoose = require('./../db') 

// 数据库的集合的基础库
const Schema = mongoose.Schema;

// 设置数据库看集合的字段以及数据类型
const schema = new Schema({
  proid: { type: String },
  proname: { type: String },
  price: { type: Number },
  proimg: { type: String },
  desc: { type: String }, // 产品的描述 + 配置的描述
  stock: { type: Number }, // 库存
  sales: { type: Number }, // 销量
  rating: { type: Number }, // 评分
  category: { type: String }, // 分类
  brand: { type: String }, // 品牌
  discount: { type: Number } // 折扣
})

// 查找或者创建数据库的集合
// 生成数据库的集合的名称为 products
module.exports = mongoose.model('Product', schema)