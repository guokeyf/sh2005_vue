var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');
var session = require('express-session');
var bodyParser = require('body-parser');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
var bannerRouter = require('./routes/banner');
var proRouter = require('./routes/pro');
var cartRouter = require('./routes/cart');
var orderRouter = require('./routes/order');
var messageRouter = require('./routes/message');


var proApi = require('./api/product')
var bannerApi = require('./api/banner')
var userApi = require('./api/user')
var cartApi = require('./api/cart')
var orderApi = require('./api/order')
var addressApi = require('./api/address')

var app = express();

var testMiddleware = (req, res, next) => {
  console.log('jahhaahahahahahahahahahaahahhaha')
  next()
}
app.use(testMiddleware)

// 自定义中间件 ---  解决跨域问题
var allowCrossDomain = (req, res, next) => {
  // get req.query post req.body
  // res
  // 所有接口均可以访问服务器
  res.header('Access-Control-Allow-Origin', '*')
  res.header('Access-Control-Allow-Headers', '*')
  next()
}
// 使用自定义跨域中间件
app.use(allowCrossDomain)

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
// app.use(express.json());
// app.use(express.urlencoded({ extended: false }));
app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use(session({
  secret : "nihao", //加密session，随便写
  cookie : {maxAge : 60*1000*30}, //设置过期时间 --- 半小时
  // cookie : {maxAge : 5000}, //测试5s
  resave : true, //强制保存session 默认为 true，建议设置成false
  saveUninitialized : false ////强制将未初始化的session存储 默认为true，建议设置成true
}))

// api 接口需要在 * 之前注册，路由从上到下匹配
app.use('/api/pro', proApi);
app.use('/api/banner', bannerApi);
app.use('/api/user', userApi);
app.use('/api/cart', cartApi);
app.use('/api/order', orderApi);
app.use('/api/address', addressApi);

app.all('*', (req, res, next) => {
  console.log('url1111111', req.url)
  // if (req.cookies.loginState === 'ok' || req.url  === '/login' || req.url === '/loginAction') {
  //   next()
  // } else {
  //   res.redirect('/login')
  // }
  if (req.session.loginState === 'ok' || req.url  === '/login' || req.url === '/loginAction') {
    next()
  } else {
    res.redirect('/login')
  }
})

app.use('/', indexRouter);
app.use('/users', usersRouter);
app.use('/banner', bannerRouter);
app.use('/cart', cartRouter);
app.use('/pro', proRouter);
app.use('/order', orderRouter);
app.use('/message', messageRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;
