var express = require('express');
var router = express.Router();
router.get('/', function(req, res, next) {
  res.render('order', {
    activeindex: 5,
    role: req.session.role
  })
});

module.exports = router;
