var express = require('express');
var md5 = require('md5');
var sql = require('./../sql')
var Admin = require('./../sql/col/Admin')
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', {
    activeindex: 0,
    role: req.session.role
  });
});

router.get('/login', (req, res, next) => {
  res.render('login')
})

router.post('/loginAction', (req, res, next) => {
  let { adminname, password } = req.body
  password = md5(password)
  sql.find(Admin, { adminname, password }, { _id: 0 }).then((data) => {
    if (data.length === 0) {
      res.redirect('/login')
    } else {

      // 登录成功，cookie保存登录状态
      // res.cookie('loginState', 'ok') // cookie
      req.session.loginState = 'ok'
      req.session.adminname = data[0].adminname
      req.session.role = data[0].role
      res.redirect('/')
    }
  })
})
module.exports = router;
