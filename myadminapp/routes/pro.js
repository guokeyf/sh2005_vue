var express = require('express');
var xlsx = require('node-xlsx')
var uuid = require('node-uuid')
var sql = require('./../sql/index');
var Product = require('./../sql/col/Product');
var router = express.Router();
// router.get('/', function(req, res, next) {
//   // 先查询数据 再 渲染页面 -- 同时需要传递 参数
//   sql.find(Product, {}, { _id: 0}).then(data => {
//     // http://localhost:3000/pro 时 渲染的时 pro.ejs 页面
//     // console.log('test11111', data)
//     res.render('pro', { // pro 代表的是 views/pro.ejs
//       activeindex: 2,
//       data
//     })
//   })
// });

router.get('/', (req, res, next) => {
  let { limit, count } = req.query
  limit = limit * 1 || 10
  count = count - 1 || 0 // 前提条件是前端传递的第一页数据的 值为 1，如果为0 则不减 1
  sql.find(Product, {}, {_id:0}).then(data1 => {
    const totalNum = data1.length
    const totalCount = Math.ceil(totalNum / limit)
    sql.paging(Product, {}, { _id: 0}, limit, count).then(data => {
      res.render('pro', {
        activeindex: 2,
        totalCount, // 总页数
        data, // 当前页的数据,
        limit,
        count,
        role: req.session.role
      })
    })
  })
  // sql.count(Product).then(num => {
  //   const totalNum = num
  //   const totalCount = Math.ceil(totalNum / limit)
  //   sql.paging(Product, {}, { _id: 0}, limit, count).then(data => {
  //     res.render('pro', {
  //       activeindex: 2,
  //       totalCount, // 总页数
  //       data, // 当前页的数据,
  //       limit,
  //       count
  //     })
  //   })
  // })
})



// 跳转到 更新产品页面
// http://localhost:3000/pro/update?proid=***
router.get('/update', (req, res, next) => {
  // 修改的是哪一条数据
  const { proid } = req.query
  sql.find(Product, { proid }, { _id: 0 }).then(data => {
    res.render('pro_update', {
      activeindex: 2, // 添加产品是保证 左侧的菜单中  商品管理是选中的状态
      proid: proid,
      proname: data[0].proname,
      proimg: data[0].proimg,
      price: data[0].price,
      stock: data[0].stock,
      sales: data[0].sales,
      discount: data[0].discount,
      desc: data[0].desc,
      category: data[0].category,
      brand: data[0].brand,
      rating: data[0].rating,
      role: req.session.role
    })
  })
})

router.post('/updateAction', (req, res, next) => {
  const obj = req.body
  obj.price *= 1
  obj.sales *= 1
  obj.stock *= 1
  obj.discount *= 1

  sql.update(Product, { proid: obj.proid }, { $set: obj }).then(() => {
    res.redirect('/pro')
  })
})

// 排序
// 排序展示所有的数据，所以总页码 totalCount 为1，限制显示的个数limit为 totalNum,当前的页码count为0
router.get('/sort', (req, res, next) => {
  let { type, num } = req.query
  num *= 1
  var sortData = {}
  sortData[type] = num
  sql.count(Product).then(num1 => {
    const totalNum = num1
    sql.sort(Product, {}, {_id: 0}, sortData).then(data => {
      res.render('pro', { // pro 代表的是 views/pro.ejs
        activeindex: 2,
        totalCount: 1,
        data,
        limit: totalNum,
        count: 0,
        role: req.session.role
      })
    })
  })
})

// 搜索
router.get('/search', (req, res, next) => {
  const {searchText} = req.query
  const reg = new RegExp(searchText)
  // 产品名称模糊查询
  // sql.find(Product, {proname: reg }, {_id: 0}).then(data => {
  //   res.render('pro', { // pro 代表的是 views/pro.ejs
  //     activeindex: 2,
  //     totalCount: 1,
  //     data,
  //     limit: data.length,
  //     count: 0
  //   })
  // })
  // 产品名称和分类模糊查询
  sql.find(Product, { $or: [{proname: reg }, { category: reg}]}, {_id: 0}).then(data => {
    res.render('pro', { // pro 代表的是 views/pro.ejs
      activeindex: 2,
      totalCount: 1,
      data,
      limit: data.length,
      count: 0,
      role: req.session.role
    })
  })
})


// 跳转到 添加产品页面
// http://localhost:3000/pro/add
router.get('/add', (req, res, next) => {
  res.render('pro_add', {
    activeindex: 2, // 添加产品是保证 左侧的菜单中  商品管理是选中的状态
    role: req.session.role
  })
})

// 提交添加商品的路由 - post提交
router.post('/addAction', (req, res, next) => {
  // res.send(req.body)
  // 准备插入的数据
  let insertData = req.body
  insertData.price *= 1
  insertData.sales *= 1
  insertData.stock *= 1
  insertData.discount *= 1
  insertData.proid = "pro_" + uuid.v1()

  sql.insert(Product, insertData).then(() => {
    res.redirect('/pro')
  })
})

// 删除操作
// http://localhost:3000/pro/delete?proid=***
router.get('/delete', (req, res, next) => {
  // 获取删除的条件
  const { proid } = req.query
  sql.delete(Product, { proid }).then(() => {
    // 跳转回列表页面 --- 重定向到商品管理界面
    res.redirect('/pro')
  })
})

// http://localhost:3000/pro/upload
router.get('/upload', (req, res, next) => {
  // __dirname 当前文件的绝对路径
  // __dirname + '/pro.xlsx' 拿到excel的地址
  // xlsx.parse() 读取excel的数据
  // [{},{},{}] 每一个对象对应一个数据表格
  const initObj = xlsx.parse(__dirname + '/pro.xlsx')[0]
  // { name: '', data: [[], [], []]}  data每一个元素代表的excel中的每一行数据
  const data = initObj.data
  // [[],[],[]] 第一个元素表示表格的表头，需要的是从第二行开始的数据

  // 遍历 data ，组成需要插入的数据
  const arr = []
  data.forEach((item, index) => {
    if (index !== 0) {
      // 组合数据 --- 参照excel表格的表头
      arr.push({
        proid: 'pro_' + uuid.v1(),
        proname: item[1],
        category: item[2],
        brand: item[3],
        proimg: item[5],
        price: item[6] * 1,
        desc: item[7],
        stock: item[8] * 1,
        sales: item[9] * 1,
        discount: item[10] * 1,
        rating: item[11] * 1
      })
    }
  })
  sql.insert(Product, arr).then(() => {
    console.log('导入数据成功')
    res.send('导入数据成功')
  })
  // res.send(arr)
})

module.exports = router;
