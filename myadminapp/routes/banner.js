var express = require('express');
const sql = require('./../sql')
const Banner = require('./../sql/col/Banner')
const uuid = require('node-uuid')
var router = express.Router();
router.get('/', function(req, res, next) {
  res.render('banner', {
    activeindex: 1,
    role: req.session.role
  })
});

router.get('/add', function(req, res, next) {
  res.render('banner_add', {
    activeindex: 1,
    role: req.session.role
  })
});


router.post('/addAction', (req, res, next) => {
  const { alt, href, img } = req.body
  sql.insert(Banner, {
    bannerid: 'banner_' + uuid.v1(),
    alt,
    href,
    img
  }).then(() => {
    res.send({
      code: '200',
      message: '上传轮播图'
    })
  })
})
module.exports = router;
