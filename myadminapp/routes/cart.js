var express = require('express');
var router = express.Router();
router.get('/', function(req, res, next) {
  if (req.session.role < 2) {
    res.render('noPermisseion')
  } else {
    res.render('cart', {
      activeindex: 4,
      role: req.session.role
    })
  }
  
});

module.exports = router;
