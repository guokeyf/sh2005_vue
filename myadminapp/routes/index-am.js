var express = require('express');
var router = express.Router();

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { 
    title: 'Express',
    name: '<mark>吴大勋</mark>',
    tel: '18813007814',
    hobby: ['篮球', '游戏', '网球'],
    sex: 1 // 1男  0 女
 });
});

router.get('/wudaxun', (req, res, next) => {
  res.send('wudaxun')
})
module.exports = router;
