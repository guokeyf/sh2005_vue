var express = require('express');
var router = express.Router();
router.get('/', function(req, res, next) {
  res.render('message', {
    activeindex: 6,
    role: req.session.role
  })
});

module.exports = router;
